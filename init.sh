#!/usr/bin/env bash
shopt -s dotglob
case "$1" in
debugi) set -x ;&
install)
	l() {
		# refer to the same file
		[[ $(realpath "$src/$1") = $(realpath "$2") ]] && return
		diff "$src/$1" "$2" -q 2>/dev/null && return

		if [[ -e "$2" ]]; then
			# path exists already
			echo "$2 exists and is not a symlink. Linking to $2.new"
			ln -s "$src/$1" "$2.new"
		else
			# create link
			ln -s "$src/$1" "$2"
		fi
	}
;;
debugu) set -x ;&
uninstall)
	l() {
		[[ $(realpath "$src/$1") = $(realpath "$2") ]] || return
		rm "$2"
	}
;;
*) echo >&2 'Please specify "install" or "uninstall"'; exit 1 ;;
esac

ldir() {
	pushd "$1" > /dev/null || return
	for file in *; do
		[[ $file = .init.sh ]] && continue
		l "$1/$file" "$2/$file"
	done
	popd > /dev/null || return
}

ldir_bin() {
	pushd "$1" > /dev/null || return
	find . -executable -type f |
	while read -r file; do
		file="${file#*/}"
		l "$1/$file" "$2/${file%.*}"
	done
	popd > /dev/null|| return
}

if [[ $# -eq 1 ]]; then
	set -- "$1"  */
fi
for dir in "${@:2}"; do
	pushd "$dir" || continue
	src="$PWD"
	# shellcheck disable=1090,1091
	[[ -f ".init.sh" ]] &&
		. ".init.sh"
	popd || continue
done
